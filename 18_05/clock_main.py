
import datetime
import numbers

def rec_all(a):
    total=['','','','','','']
    for i in range(0,6):
        total[i]=(numbers.recognize(a[i]))
    return total


def printing(a):
    t=rec_all(a)
    i=0
    for k in range(0,7):
        print(t[i][k]+" "+t[i+1][k]+"    "+t[i+2][k]+" "+t[i+3][k]+"    "+t[i+4][k]+" "+t[i+5][k])
        k=k+1
    return 0

def the_date():
   s=datetime.datetime.now()
   clock=datetime.datetime.strftime(s,"%Y-%m-%d %H:%M:%S")
   return (clock[11:19])

def splitting(a):
 b=list(a.replace(':', ''))
 return b

def main():
    a=splitting(the_date())
    printing(a)
    print('\n')
    while True:
        b=splitting(the_date())
        if not a==b:
            printing(b)
            print('\n')
            a=b
  

if __name__ == "__main__":
    main()
    pass