def zero():
    a='■'
    b=" "
    s=['','','','','','','']
    s[0]=b+a*3+b
    s[1]=a+b*3+a
    s[2]=a+b*3+a
    s[3]=a+b*3+a
    s[4]=a+b*3+a
    s[5]=a+b*3+a
    s[6]=b+a*3+b
  
    return s

def one():
    a='■'
    b=" "
    s=['','','','','','','']
    s[0]=b*2+a+b*2
    s[1]=b+a*2+b*2
    s[2]=b*2+a+b*2
    s[3]=b*2+a+b*2
    s[4]=b*2+a+b*2
    s[5]=b*2+a+b*2
    s[6]=b+a*3+b
    
    return s

def two():
    a='■'
    b=" "
    s=['','','','','','','']
    s[0]=b+a*3+b*2
    s[1]=a+b*3+a
    s[2]=b*3+a*2
    s[3]=b*3+a+b
    s[4]=b*2+a+b*2
    s[5]=b+a+b*3
    s[6]=a*5
    #print('\n',s[0],'\n',s[1],'\n',s[2],'\n',s[3],'\n',s[4],'\n',s[5],'\n',s[6])
    return s

def three():
    a='■'
    b=" "
    s=['','','','','','','']
    s[0]=b+a*3+b
    s[1]=a+b*3+a
    s[2]=b*3+a+b
    s[3]=b*2+a+b*2
    s[4]=b*3+a+b
    s[5]=a+b*3+a
    s[6]=b+a*3+b*2
    #print('\n',s[0],'\n',s[1],'\n',s[2],'\n',s[3],'\n',s[4],'\n',s[5],'\n',s[6])
    return s

def four():
    a='■'
    b=" "
    s=['','','','','','','']
    s[0]=b*4+a
    s[1]=b*3+2*a
    s[2]=b+a+b*2+a
    s[3]=a*5
    s[4]=b*4+a
    s[5]=b*4+a
    s[6]=b*4+a
    #print('\n',s[0],'\n',s[1],'\n',s[2],'\n',s[3],'\n',s[4],'\n',s[5],'\n',s[6])
    return s

def five():
    a='■'
    b=" "
    s=['','','','','','','']
    s[0]=a*5
    s[1]=a+b*4
    s[2]=a*4+b  
    s[3]=b*4+a
    s[4]=b*4+a
    s[5]=a+b*3+a
    s[6]=b+a*3+b
    #print('\n',s[0],'\n',s[1],'\n',s[2],'\n',s[3],'\n',s[4],'\n',s[5],'\n',s[6])
    return s

def six():
    a='■'
    b=" "
    s=['','','','','','','']
    s[0]=b*2+a*2+b
    s[1]=b+a+b*3
    s[2]=a+b*4 
    s[3]=a*4+b
    s[4]=a+b*3+a
    s[5]=a+b*3+a
    s[6]=b+a*3+b
    #print('\n',s[0],'\n',s[1],'\n',s[2],'\n',s[3],'\n',s[4],'\n',s[5],'\n',s[6])
    return s

def seven():
    a='■'
    b=" "
    s=['','','','','','','']
    s[0]=a*5
    s[1]=b*4+a
    s[2]=b*3+a+b
    s[3]=b*2+a+b*2
    s[4]=b+a+b*3
    s[5]=b+a+b*3
    s[6]=b+a+b*3
    #print('\n',s[0],'\n',s[1],'\n',s[2],'\n',s[3],'\n',s[4],'\n',s[5],'\n',s[6])
    return s

def eight():
    a='■'
    b=" "
    s=['','','','','','','']
    s[0]=b+a*3+b
    s[1]=a+b*3+a
    s[2]=a+b*3+a
    s[3]=b+a*3+b
    s[4]=a+b*3+a
    s[5]=a+b*3+a
    s[6]=b+a*3+b
    #print('\n',s[0],'\n',s[1],'\n',s[2],'\n',s[3],'\n',s[4],'\n',s[5],'\n',s[6])
    return s

def nine():
    a='■'
    b=" "
    s=['','','','','','','']
    s[0]=b+a*3+b
    s[1]=a+b*3+a
    s[2]=a+b*3+a 
    s[3]=b+a*4
    s[4]=b*4+a
    s[5]=b*3+a+b
    s[6]=b+a*2+b*2
    #print('\n',s[0],'\n',s[1],'\n',s[2],'\n',s[3],'\n',s[4],'\n',s[5],'\n',s[6])
    return s

def recognize(a):
    zero_n=zero()
    two_n=two()
    one_n=one()
    three_n=three()
    four_n=four()
    five_n=five()
    six_n=six()
    seven_n=seven()
    eight_n=eight()
    nine_n=nine()

    num=['']
    if a=='1':
     num=one_n
    if a=='2':
     num=two_n
    if a=='3':
     num=three_n
    if a=='4':
     num=four_n
    if a=='5':
     num=five_n
    if a=='6':
     num=six_n
    if a=='7':
     num=seven_n
    if a=='8':
     num=eight_n
    if a=='9':
     num=nine_n
    if a=='0':
     num=zero_n
    #print('\n',num[0],'\n',num[1],'\n',num[2],'\n',num[3],'\n',num[4],'\n',num[5],'\n',num[6])
    return num