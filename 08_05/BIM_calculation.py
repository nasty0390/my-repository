
def new_user_creation():
    u = dict(u_name=input("ФИО "), u_weight=input("Вес "), u_height=input("Рост "), u_age=input("Возраст "))
    return u

def list_creating():
    k=0
    global users
    users={}
    while input("Создать нового пользователя? (Y/N)")=="Y" :
        k=k+1
        users[k]=new_user_creation()
      
    return k

def show_users_list(k):
    z=0
    print ("Список созданных пользователей: ")
    for i in range(0, k): 
        z=z+1
        print ("Пользователь "+str(z)+": "+users[(z)]['u_name'])


def get_user_data(k):
    show_users_list(k)
    id=input("Введите номер пользователя для отображения информации о нем: ")
    return id

    
def bmi_calculation(k):
    weight=(users[(k)]['u_weight'])
    height=(users[(k)]['u_height'])
    Index=int(int(weight)/(int(height)/100)**2) 
    return Index

def scale_printing(a):
    min_b=15 
    max_b=50 
    scale=(str(min_b) +'='*(a) +'|' +'='*(max_b-a) +str(max_b))
    return print(scale)

def bmi_record(k):
    for i in range(1, k+1):  
        users[(i)]['u_index']=bmi_calculation(i)
   
def recommendations(a):
    if users[(a)]['u_index']>18 and users[(a)]['u_index']<25:
         users[(a)]['u_result']="вес в норме"
    if users[(a)]['u_index']>=25 :
         users[(a)]['u_result']="нужно похудеть"
    if users[(a)]['u_index']<=18 :
         users[(a)]['u_result']="нужно набрать вес"
    print("Рекомендация: "+str(users[(a)]['u_result']))
    I=bmi_calculation(a)
    scale_printing(I)
    return users[(a)]['u_result']
    
def info(a):
    print ("1. Имя:"+ (users[(a)]['u_name']))
    print ("2. Вес:"+ (users[(a)]['u_weight']))
    print ("3. Рост:"+ (users[(a)]['u_height']))
    print ("4. Возраст:"+ (users[(a)]['u_age']))
    print ("5. Индекс массы тела:"+ str((users[(a)]['u_index'])))

def calories_calc(a):
    Protein=int(users[(a)]['u_weight'])*2
    Carb=int(users[(a)]['u_weight'])*2
    Fat=int(users[(a)]['u_weight'])*1
    Calories=Protein*4+Carb*9+Fat*4
    return print("Белки: "+str(Protein)+" жиры: "+str(Fat)+" углеводы: "+str(Carb)+" калории: "+str(Calories)) 

    
def analytics(k):
    for i in range(1, k+1): 
        if users[(i)]['u_index']>=25 or users[(i)]['u_index']<=18:
            z=input("У пользователя "+users[(i)]['u_name']+" проблемы с весом. Расчитать рекомендуемый ежедневный колораж?(Y/N)")
            if z=="Y" or z=="y":
                calories_calc(i)

def delete_user(k):
    z=input("Хотите удалить пользователя из списка? (Y/N)") 
    if z=="Y" or z=="y":
        show_users_list(k)
        id=int(input("Введите номер пользователя для удаления: "))
        del (users[id])
    

def main():
    l=list_creating() #создаем словарь с введенными пользователями
    bmi_record(l) #расчитываем индекс и добавляем его в словарь
    user_number=int(get_user_data(l)) #получаем номер пользователя для отображения
    info(user_number)#выводим информацию о пользователе
    recommendations(user_number)#выводим рекомендацию указаному пользователю
    analytics(l)#анализируем всех пользователей на проблемы с весом
    delete_user(l)#удаляем пользователя
   
    


if __name__ == "__main__":
    main()
    pass






