class plant:
    def __init__(self,a,color,envir):
        self.a=a
        self.color=color
        self.envir=envir

    def flower(self):
        print(f"This plant is a {self.a} and have {self.color} color")
    
fl1=plant('poppy','red','outdoor')
fl1.flower()
